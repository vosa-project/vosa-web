---
layout: page
title: Õpiruumid - VÕSA
permalink: /opiruumid-vosa/
public: true
---

#### VÕSA projekti raames valminud õpiruumid kuuluvad [MIT litsentsi](https://opensource.org/licenses/MIT) alla ning on kasutatavad ja muudetavad kõigile.

## DEVOPS File Management

Praktilisi Linuxi käsureal teostatavaid failide haldamise oskusi ja teadmisi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur vaadelda, luua, muuta ning eemaldada Linuxi käsureal faile ja kaustu; hallata failiõigusi ning failide omanikke ja gruppe.

## DEVOPS Disk Management

Praktilisi Linuxi käsureal teostatavaid ketta haldamise oskusi ja teadmisi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur partitsioneerida kettaid kasutades fdisk tööriista; optimiseerida ning sisse lülitada saaleala; luua failisüsteeme; külge ja lahti haakida kettaid; võimaldada saaleala ja külgehaakimist serveri alglaadimisel.

## DEVOPS Storage Management LVM

Praktilisi Linuxi käsureal teostatavaid ketta haldamise oskusi ja teadmisi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur luua LVMi kettagruppe ja kettajagusid ning seadistada nende kasutamist alglaadimisel. 

## DEVOPS Storage Management Partitions

Praktilisi Linuxi käsureal teostatavaid ketta haldamise oskusi ja teadmisi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur luua kahte erinevat tüüpi kettajagude tabelit ja kettajagu kahe erineva programmiga ning seadistatada nende kasutamist alglaadimisel. 

## DEVOPS Storage Management RAID

Praktilisi Linuxi käsureal teostatavaid ketta haldamise oskusi ja teadmisi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur luua erinevat tüüpi RAIDi massiive (sisuliselt plokkseade) ja kettajagusid ning seadistatada nende kasutamist alglaadimisel.

## OpenVPN AS Installation

Praktilise Linuxi käsureal OpenVPN installeerimise õpetuse labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur installeerida OpenVPN AS-i; kirjeldada VPN-i kasutamise eeliseid.

## System Compromised (Küberturbe algtaseme labor)

Linuxi keskkonnas põhilisi turbe oskusi, teadmisi ja harjumusi õpetav labor. Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur alla laadida, paigaldada ja eemaldada pakke; uurida võrgu statistikat ning operatsioonisüsteemi protsesse; muuta turvalisemaks privaatvõtmega autentimine. Lisaks aitab õpiruum arendada turvalisemat mõtteviisi küberis.

## DEVOPS User Management
Praktilisi Linuxi käsureal teostatavaid kasutajate haldamise oskusi ja teadmisi õpetav labor.
Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel teab ja oskab õppur vaadelda, luua, muuta ning eemaldada Linuxi käsureal lokaalseid kasutajaid; hallata kasutajate paroole ning nendega seotud gruppe.

## DEVOPS Final Test
Praktilisi Linuxi käsureal teostatavaid administreerimise oskusi ja teadmisi hindav labor.
Õpiruum on suunatud algajale ja ka edasijõudnud Linuxi käsurea valdajaile. Labor koosneb ligi 20nest praktilisest ülesandest, millega erinevate valdkondade teadmisi hinnata. Õpiruumi läbimise järel oskab õppur hinnata oma baasoskui Linuxi käsureal administreerimisülesannete tegemiseks.

## HTTPS Security
Veebiserveri tarkvarade (Apache ja Nginx) paigaldamist ja konfigureerimist ning HTTPS protokolli turvalist realiseerimist ja testimist õpetav labor. Lisaks käsitleb labor mõningaid levinud HTTPS protokolli ja selle realiseerimise haavatavusi.
Õpiruum on suunatud õppurile, kellel on algteadmised Linuxi käsureast ning tööriistadest. Õpiruumi läbimise järel oskab õppur paigaldada ja seadistada kahte enimlevinud veebiserveri tarkvara ning nendel HTTPS protokolli realiseerida.

