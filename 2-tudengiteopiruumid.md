---
layout: page
title: Õpiruumid - Tudengid
permalink: /opiruumid-tudengid/
public: true
---

#### Tudengite arendatud õpiruumid kuuluvad [MIT litsentsi](https://opensource.org/licenses/MIT) alla.

## Monitoring

IT Süsteemide monitoorimise põhimõtet ja seadistamist õpetav labor. Õpiruum on suunatud algajatele ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel õppur teab ja oskab seadistada ning kasutada Zabbix monitoorimistarkvara.

## Vyos - Getting Started

Vyos ruuterit tutvustav labor, kus õpetatakse selle eripärasid ning seda käsureal seadistama ja kasutama. Õpiruum on suunatud algajatele ja ka edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel õppur teab ja oskab algseadistada Vyos ruuterit.

## Vyos - Configuring Services and NAT

Vyos ruuterile erinevate teenuste seadistamist õpetav labor. Õpiruum on suunatud edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel õppur teab ja oskab seadistada erinevaid teenuseid nagu NAT, DHCP ja DNS.

## Vyos - Firewall

Vyos ruuteri tulemüüri seadistamist õpetav labor. Õpiruum on suunatud edasijõudnud Linuxi käsurea valdajaile. Õpiruumi läbimise järel õppur teab ja oskab kuidas seadistada tulemüüri väikese ettevõtte võrgu jaoks.

