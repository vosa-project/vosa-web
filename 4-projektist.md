---
layout: page
title: Projektist
permalink: /projektist/
public: true
---

# Projekti "Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine"

Eesti Infotehnoloogia Kolledž alustas 2016 aasta suvest uue arendusprojektiga „Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“, projekti akronüümina kasutame lühendit (VÕSA). 2017 aasta 1 augustist jätkas projektiga Tallinna Tehnikaülikooli Infotehnoloogia Kolledž.

## Projekti eesmärk
Arendusprojekti VÕSA eesmärk on luua uudne, õppetegevust toetav ja laiendav virtuaalne õpikeskkond, mis võimaldab muuta IKT õpet praktilisemaks, paindlikumaks ja õppijat toetavamaks.

![EL Regionaalarengu Fond](https://raw.githubusercontent.com/magavdraakon/V-SA/master/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

## Projekti üldinfo
Projekti käigus loome mängulise, reaalseid probleeme simuleeriva õpikeskkonna, mis muudab igava kodutöö huvitavaks ja motiveerib õppureid rohkem pingutama.
Õppur siseneb simulatsioonikeskkonda veebisirvija abil ning satub õpiruumi, kus on virtualiseeritud e-teenused, serverid, tööjaamad, kuid ka küberkurikaelad, kes õppuri kaitstavat simulatsiooni ründavad. Õppur leiab ja lahendab probleeme ning kaitseb oma teenuseid ja süsteeme.

Simulatsioonikeskkond võimaldab õpetada nii IT süsteemide administreerimise, IT süsteemide arendamise kui ka küberturbe tehnoloogiate õppekavade õppureid.

Projekti tulemusena valmivat simulatsioonikeskkonda saavad kasutada ka teised osapooled, mis aitab suurendada koostööd ka erinevate õppeasutuste ning tööandjate vahel.

Eesti Infotehnoloogia Kolledž oli 2000. aastal loodud mittetulunduslik rakenduskõrgkool, mida haldas Hariduse Infotehnoloogia Sihtasutus (HITSA). TTÜ IT Kolledž loodi 1. augustil 2017 Tallinna Tehnikaülikooli ja Eesti Infotehnoloogia Kolledži ühinemisel, mille järgselt kuulub IT Kolledž TTÜ infotehnoloogia teaduskonna koosseisu. TTÜ IT Kolledžis saab omandada rakendusliku kõrghariduse õppekavadel IT süsteemide administreerimine, IT süsteemide arendus, Infosüsteemide analüüs ja ingliskeelsel õppekaval Cyber Security Engineering (Küberturbe tehnoloogiad).

Projekti rahastatakse Euroopa Regionaalarengu Fondi vahenditest “Ühtekuuluvuspoliitika fondide rakenduskava 2014−2020″ prioriteetse suuna “Kasvuvõimeline ettevõtlus ja rahvusvaheliselt konkurentsivõimeline teadus- ja arendustegevus” meetme “Eesti T&A rahvusvahelise konkurentsivõime suurendamine ja osalemine üle-euroopalistes teadusalgatustes” tegevuse “Institutsionaalne arendusprogramm TA asutustele ja kõrgkoolidele” raames.

**Margus Ernits, VÕSA arendusjuht**
Tallinna Tehnikaülikooli Infotehnoloogia Kolledž
margus.ernits@itcollege.ee

**Roland Kaur, VÕSA õpiruumi spetsialist**
Tallinna Tehnikaülikooli Infotehnoloogia Kolledž
roland.kaur@itcollege.ee

**Alvar Kurrel, VÕSA projektijuht**
Tallinna Tehnikaülikool
alvar.kurrel@ttu.ee

