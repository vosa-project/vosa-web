---
layout: post
title:  "IT Kolledži arendusprojekti raames valmisid esimesed laborid"
date:   2016-12-16 08:00
categories: Õpiruumid
permalink: /archivers/esimesed-laborid.html
---

Käesoleva aasta augustis alanud IT Kolledži arendusprojekti **„Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“** (VÕSA) esimesed laborid valmisid novembri lõpus. Selle aja jooksul on keskkonna vastu huvi tundnud üle 50-ne inimese, kellest mitmed on jaganud ka väärtuslikku tagasisidet.

HTTPS Security, DEVOPS Final Test ja DEVOPS User Management, mida huvilistel on olnud võimalik proovida, on nii Linuxi käsureal administreerimise kui turvalisusega seotud algtaseme teadmisi mõõtvad ja hindavad laborid. Senine tagasiside on olnud igati positiivne ja konstruktiivne, mis on innustanud laboreid veelgi edasi arendama ning täiendama. Saadud järeldused ei ole veel lõplikud, kuna laborite testimine ning tagasiside korjamine jätkuvad ka edaspidi. Laboreid on endiselt võimalik proovida registreerides [SIIN](https://goo.gl/forms/r1FuEcXR2VYDsuRD2).

Senisest huvist ning tagasisidest innustatuna toimuvad 2017. aasta jaanuaris neli platvormi puudutavat seminari. Seminaride eesmärk on:
* Tutvustada VÕSA projekti kaugemaid eesmärke ning tulemusi.
* Kaasata projekti uusi virtuaallaborite süsteemi paigaldajaid ning laborite arendajaid.
* Demonstreerida virtuaallaborite süsteemi kasutatavust õppetöös.

Nelja seminari jooksul käiakse läbi kõik vajalikud etapid laborite loomiseks ja kasutamiseks õppetöös. Täiendavalt käsitletakse virtuaallaborite süsteemi paigaldust oma ettevõtte taristusse. Seminarid toimuvad eesti keeles.

**Seminaride ajakava:**
* 11. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217
* 18. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 219
* 25. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217
* 1. veebruar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217

**Palume seminarile tulla isikliku sülearvutiga.**

**Seminarile toimub eelregistreerimine [SIIN](https://goo.gl/forms/BDXCKWnxJShQT4qN2)*. Kohtade arv on piiratud ja kohtade täitumisel sulgeme registreerimise.

Eesti Infotehnoloogia Kolledži arendusprojekti **„Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“* (VÕSA) eesmärk on luua uudne, õppetegevust toetav ja laiendav virtuaalne õpikeskkond, mis võimaldab muuta IKT õpet praktilisemaks, paindlikumaks ja õppijat toetavamaks.

Projekti rahastatakse Euroopa Regionaalarengu Fondi vahenditest “Ühtekuuluvuspoliitika fondide rakenduskava 2014−2020″ prioriteetse suuna “Kasvuvõimeline ettevõtlus ja rahvusvaheliselt konkurentsivõimeline teadus- ja arendustegevus” meetme “Eesti T&A rahvusvahelise konkurentsivõime suurendamine ja osalemine üle-euroopalistes teadusalgatustes” tegevuse “Institutsionaalne arendusprogramm TA asutustele ja kõrgkoolidele” raames.

**Eesti Infotehnoloogia Kolledž** on 2000. aastal loodud mittetulunduslik rakenduskõrgkool, mida haldab Hariduse Infotehnoloogia Sihtasutus (HITSA). IT Kolledžis saab omandada rakendusliku kõrghariduse õppekavadel IT süsteemide administreerimine, IT süsteemide arendus, Infosüsteemide analüüs ja ingliskeelsel õppekaval Cyber Security Engineering (Küberturbe tehnoloogiad).

Lisainfo:

**Katrin Loodus, VÕSA projekti õpiruumide spetsialist**
Eesti Infotehnoloogia Kolledž
katrin.loodus@itcollege.ee

**Karin Kuldmets, VÕSA ja täiendusõppe projektijuht**
Eesti Infotehnoloogia Kolledž
+372 6285 833; +372 5647 0560
karin.kuldmets@itcollege.ee
www.itcollege.ee

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

