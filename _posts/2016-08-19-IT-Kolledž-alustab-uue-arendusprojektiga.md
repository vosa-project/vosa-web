---
layout: post
title:  "IT Kolledž alustab uue arendusprojektiga"
date:   2016-08-19 08:00
categories: Projekt
permalink: /archivers/vosa-alustab.html
---

**Eesti Infotehnoloogia Kolledž alustab uue arendusprojektiga „Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“ (VÕSA). Projekti eesmärk on luua uudne, õppetegevust toetav ja laiendav virtuaalne õpikeskkond, mis võimaldab muuta IKT õpet praktilisemaks, paindlikumaks ja õppijat toetavamaks.**

„Tihti on õpe liiga teoreetiline ja õppurid ei seosta õpitut praktiliste IKT probleemidega,“ rääkis **VÕSA projekti arendusjuht Margus Ernits**. „Projekti käigus loome mängulise, reaalseid probleeme simuleeriva õpikeskkonna, mis muudab igava kodutöö huvitavaks ja motiveeriks õppureid rohkem pingutama,“ lisas Ernits.

Õppur siseneb simulatsioonikeskkonda veebisirvija abil ning satub õpiruumi, kus on virtualiseeritud e-teenused, serverid, tööjaamad, kuid ka küberkurikaelad, kes õppuri kaitstavat simulatsiooni ründavad. Õppur leiab ja lahendab probleeme ning kaitseb oma teenuseid ja süsteeme.

Simulatsioonikeskkond võimaldab õpetada nii IT süsteemide administreerimise, IT süsteemide arendamise kui ka küberturbe tehnoloogiate õppekavade õppureid.

Projekti tulemusena valmivat simulatsioonikeskkonda saavad kasutada ka teised osapooled, mis aitab suurendada koostööd ka erinevate õppeasutuste ning tööandjate vahel.

Projekti rahastatakse Euroopa Regionaalarengu Fondi vahenditest “Ühtekuuluvuspoliitika fondide rakenduskava 2014−2020″ prioriteetse suuna “Kasvuvõimeline ettevõtlus ja rahvusvaheliselt konkurentsivõimeline teadus- ja arendustegevus” meetme “Eesti T&A rahvusvahelise konkurentsivõime suurendamine ja osalemine üle-euroopalistes teadusalgatustes” tegevuse “Institutsionaalne arendusprogramm TA asutustele ja kõrgkoolidele” raames.

**Eesti Infotehnoloogia Kolledž** on 2000. aastal loodud mittetulunduslik rakenduskõrgkool, mida haldab Hariduse Infotehnoloogia Sihtasutus (HITSA). IT Kolledžis saab omandada rakendusliku kõrghariduse õppekavadel IT süsteemide administreerimine, IT süsteemide arendus, Infosüsteemide analüüs ja ingliskeelsel õppekaval Cyber Security Engineering (Küberturbe tehnoloogiad).

Lisainfo:

**Katrin Loodus, VÕSA projekti õpiruumide spetsialist**  
Eesti Infotehnoloogia Kolledž  
katrin.loodus@itcollege.ee

**Karin Kuldmets, VÕSA ja täiendusõppe projektijuht**  
Eesti Infotehnoloogia Kolledž  
+372 6285 833; +372 5647 0560  
karin.kuldmets@itcollege.ee  
www.itcollege.ee

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)
