---
layout: post
title:  "16. jaanuaril toimus TTÜ IT Kolledžis seminar VÕSA projekti ja selle võimaluste tutvustamiseks."
date:   2018-01-22 12:00
categories: Seminarid
permalink: /archivers/ttu-itk-salvestus.html
---

{% include youtubePlayer.html id="W_ciyiNdktg" %}

**16. jaanuaril toimus TTÜ IT Kolledžis VÕSA projekti seminar, kus tutvustati kõikidele huvilistele selle projekti võimalusi ja eesmärke. Üldise ülevaate projektist andis selle arendusjuht Margus Ernits, erinevaid võimalusi ja õpiruume tutvustasid Artur Ovtšinnikov ja Roland Kaur ning kasutajakogemustest rääkis Edmund Laugasson.**

Esitluste slaidid:

[Margus Ernits](https://files.itcollege.ee/VOSA-projekti-tutvustus-sissejuhatus.pdf)  
[Edmund Laugasson](http://enos.itcollege.ee/~edmund/vosa/2018-01-16_VOSA-seminar_Edmund.pdf)

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

