---
layout: post
title:  "Tehnikaülikooli virtuaalseid õpiruume saavad kasutada ka teiste kõrg- ja kutsekoolide õppejõud"
date:   2018-01-05 08:00
categories: Seminarid
permalink: /archivers/ttu-itk-seminarid.html
---

**Tallinna Tehnikaülikooli IT kolledžil on valminud uued virtuaalsed õpiruumid, kus saavad oma IKT praktilisi töid läbi viia ka teiste kõrg- ja kutsekoolide õppejõud. Õpiruumide kasutamise võimalusi tutvustatakse 16. jaanuaril kell 14:00 TTÜ  IT Kolledžis seminaril, kus osalemiseks tuleb eelnevalt registreeruda [SIIN](https://goo.gl/forms/F14WDPO1jsFHekZt1).**

Õpiruumide kasutamine on tasuta ning nende loomist on rahastanud Euroopa Regionaalarengu Fond.

Uutes õpiruumides saab viia läbi praktilist IT õppetööd, milles osalemiseks on vajalik vaid internetiühendusega arvutit. Vajalik infrastruktuur on olemas virtuaalses õpiruumis. Selline töökorraldus muudab õppetöö paindlikumaks ning selles saab osaleda ka näiteks kodust lahkumata.

„Õppetöös osalemiseks pole vaja oma arvutisse installeerida vajalikke e-teenuseid, need on keskkonnas olemas. Näiteks küberturbe õppimiseks on liivakast, kus on olemas nii küberkurikaelad kui ka arvutikasutajad, et õppida kaitsma ja looma teenuseid meie digitaalseks eluks,“ kinnitab Margus Ernits TTÜ  IT kolledžist.

Lisaks on need õpiruumid mugavad ka õppejõule, kes saab anda kohest ning interaktiivset tagasisidet, ka on süsteem käepärane kaugõppijale.

Nii saab näiteks õpetada IT süsteemide administreerimise, arendamise või küberturbe õppureid nii kutse- kui ka kõrgkoolide päevases ja kaugõppes.

Õpiruume on juurde loonud ka neid kasutanud tudengid.

Esimesed IT kolledži virtuaalsed õpiruumid loodi 2016. aasta augustis arendusprojekti „Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“ (VÕSA) raames ning seda on kasutanud rohkem kui 400 tudengit. VÕSA õpiruume saavad kasutada Eesti kõrg- ja kutsekoolide õppejõud praktiliste IKT laborite läbiviimiseks muutes seeläbi IKT õpet praktilisemaks, paindlikumaks ja õppijat toetavamaks.

Lisainfo:  
Projekti veebileht: http://vosa.itcollege.ee

**Margus Ernits**, VÕSA projekti arendusjuht  
TTÜ IT Kolledž  
margus.ernits@itcollege.ee

**Alvar Kurrel**, VÕSA projektijuht  
Tallinna Tehnikaülikool  
alvar.kurrel@ttu.ee  

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

