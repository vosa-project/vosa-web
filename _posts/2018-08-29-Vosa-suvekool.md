---
layout: post
title:  "21.-22. augustil toimus Sausti mõisas VÕSA suvekool"
date:   2018-08-29 09:00
categories: Seminarid
permalink: /archivers/ttu-itk-suvekool.html
---

**21.-22. augustil toimus Sausti mõisas VÕSA suvekool, kuhu tulid kokku TTÜ ja TTÜ IT Kolledži õppejõud, tudengid ning koostööpartnerid. Suvekoolil süveneti õpiruumide loomisele.**

Esimesel päeva hommikul andis VÕSA projekti arendusjuht, Margus Ernits, põhjaliku ülevaate projekti hetke seisust, I-Tee platformist ja virtuaalsete õpiruumide ülesehitustest. Enne lõunat mõeldi kõikide suvekoolil osalejatega välja 4 teemat, millest võiks luua virtuaalsed õpiruumid. Nendeks teemadeks olid: 

* *Docker*'i paigaldamine ja konteineri loomine;
* võrkude konfigureerimine;
* *iptables*'i seadistamine;
* *vim exit room*.


Peale lõunat jaguneti gruppideks, valiti endale üks neljast teemast ning asuti õpiruumi looma. Laborite kallal töötati õhtutundideni.

Teisel päeval jätkati tööd õpiruumide tehnilise poolega. Enne lõunat tegi iga grupp kokkuvõtte oma õpiruumi seisust, selle arendamisel esinenud probleemidest ja kõigest õpitust. Kõik toimus vabas arutelu vormis.

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

