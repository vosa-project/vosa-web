---
layout: post
title:  "19. veebruaril toimus TTÜ IT Kolledžis VÕSA projekti raames seminar tudengitele"
date:   2018-02-21 09:00
categories: Seminarid
permalink: /archivers/ttu-itk-tudengite-seminar.html
---

**19. veebruaril toimus TTÜ IT Kolledžis VÕSA projekti raames seminar tudengitele, kus räägiti lõputöö kirjutamisest ja kaitsmisest. Projekti arendusjuht Margus Ernits andis lühikese ülevaate VÕSA projektist, rääkis lõputööde kirjutamise ja kaitsmise meelespeadest ning pakkus välja VÕSA projekti raames erinevaid lõputöö valdkondi.**

Seminari materjalid:

* [**Seminari salvestus**](https://echo360.org.uk/media/2a697e64-8f8a-4875-b076-720df1accd71/public)

* [**Margus Ernitsa slaidid**](https://files.itcollege.ee/VOSA-projekti-l%C3%B5put%C3%B6%C3%B6de-seminar.pdf)  

* [**Pakutavad lõputöö valdkonnad**](https://docs.google.com/document/d/1Amp_Th6LcbK34yLGvl51OXzKniJStzAlFdqgoS0UiCQ/edit?usp=sharing) (Future Labs)


![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

