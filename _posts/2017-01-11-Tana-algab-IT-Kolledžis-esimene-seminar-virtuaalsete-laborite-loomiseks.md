---
layout: post
title:  "Täna algab IT Kolledžis esimene seminar virtuaalsete laborite loomiseks"
date:   2017-01-11 08:00
categories: Seminarid
permalink: /archivers/esimesed-seminarid.html
---

**Täna, 11. jaanuaril toimub Eesti Infotehnoloogia Kolledži arendusprojekti „Virtuaalsete õpiruumide ja simulatsioonikeskkonna loomine ja juurutamine“ (VÕSA) raames esimene seminar virtuaalsete laborite loomiseks. Arendusprojekti VÕSA eesmärk on luua uudne, õppetegevust toetav ja laiendav virtuaalne õpikeskkond, mis võimaldab muuta IKT õpet praktilisemaks, paindlikumaks ja õppijat toetavamaks.**

Kokku viiakse neljal järjestikusel kolmapäeval läbi neli seminari, mille eesmärk on:
* Kaasata projekti uusi virtuaallaborite süsteemi paigaldajaid ning laborite arendajaid.
* Demonstreerida virtuaallaborite süsteemi kasutatavust õppetöös.
* Tutvustada VÕSA projekti kaugemaid eesmärke ning tulemusi.

Nelja seminari jooksul käiakse läbi kõik vajalikud etapid laborite loomiseks ja kasutamiseks õppetöös. Täiendavalt käsitletakse virtuaallaborite süsteemi paigaldust oma ettevõtte taristusse. Ühtlasi valmivad laborimaterjalid õppetöö hõlbustamiseks. Seminarid toimuvad eesti keeles.

Tänase seisuga on platvormil projekti käigus välja töötatud kolm laborit *HTTPS Security*, *DEVOPS Final Test* ja *DEVOPS User Management*, mida huvilistel on olnud võimalik proovida ja senine tagasiside on olnud valdavalt positiivne. Seminaridele toimus eelregistreerimine ja huvilisi on nii haridusasutustest kui avalikust ja erasektorist.

Kõik seminarid salvestatakse ning salvestusi jagatakse pärast iga seminari lõppu kõigile huvilistele.

Juhul kui soovid veel seminaril osaleda, registreeri end [SIIN](https://goo.gl/forms/BDXCKWnxJShQT4qN2).

**Seminaride ajakava:**

* 11. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217,
**VÕSA projekti ja virtuaallaborite süsteemi i-tee tutvustus**. [SALVESTUS: I seminar](https://www.youtube.com/watch?v=lr8-TkrkVFY&feature=youtu.be).
* 18. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 219,
**Virtual Teaching Assistant vahendi tutvustus**. [SALVESTUS: II seminar](https://www.youtube.com/watch?v=wq-rvQ9VUqk&feature=youtu.be).
* 25. jaanuar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217,
**Automaatkontrollide tutvustus ja loomine**. [SALVESTUS: III seminar](https://www.youtube.com/watch?v=waPUL1TY_xY&feature=youtu.be).
* 1. veebruar 2017 kell 14:00 – 17:00 IT Kolledžis ruumis 217,
**Virtuaallabori i-tee keskkonna paigaldamine ettevõtte infrastruktuuri**

Projekti rahastatakse Euroopa Regionaalarengu Fondi vahenditest “Ühtekuuluvuspoliitika fondide rakenduskava 2014−2020″ prioriteetse suuna “Kasvuvõimeline ettevõtlus ja rahvusvaheliselt konkurentsivõimeline teadus- ja arendustegevus” meetme “Eesti T&A rahvusvahelise konkurentsivõime suurendamine ja osalemine üle-euroopalistes teadusalgatustes” tegevuse “Institutsionaalne arendusprogramm TA asutustele ja kõrgkoolidele” raames.

**Eesti Infotehnoloogia Kolledž** on 2000. aastal loodud mittetulunduslik rakenduskõrgkool, mida haldab Hariduse Infotehnoloogia Sihtasutus (HITSA). IT Kolledžis saab omandada rakendusliku kõrghariduse õppekavadel IT süsteemide administreerimine, IT süsteemide arendus, Infosüsteemide analüüs ja ingliskeelsel õppekaval Cyber Security Engineering (Küberturbe tehnoloogiad).

Lisainfo:

**Katrin Loodus, VÕSA projekti õpiruumide spetsialist**
Eesti Infotehnoloogia Kolledž
katrin.loodus@itcollege.ee

**Karin Kuldmets, VÕSA ja täiendusõppe projektijuht**
Eesti Infotehnoloogia Kolledž
+372 6285 833; +372 5647 0560
karin.kuldmets@itcollege.ee
www.itcollege.ee

![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

