---
layout: post
title:  "On the 19th of March there was a VÕSA Project seminar held in TUT IT College for foreign students"
date:   2018-03-19 09:00
categories: Seminarid
permalink: /archivers/ttu-itk-student-seminar.html
---

**On the 19th of March there was a seminar held in TUT IT College regarding the VÕSA project. Significant aspects about writing and defending a diploma thesis were covered. That includes common mistakes, important suggestions etc. In addition a few possible thesis topics were presented as a part of the VÕSA Project.**

Seminar materials:

* [**Margus Ernits slides**](https://files.itcollege.ee/VOSA-projekti-l%C3%B5put%C3%B6%C3%B6de-seminar-en.pdf)  

* [**Possible diploma thesis topics**](https://docs.google.com/document/d/1Amp_Th6LcbK34yLGvl51OXzKniJStzAlFdqgoS0UiCQ/edit?usp=sharing) (Future Labs)


![ELReg](/EL_Regionaalarengu_Fond_horisontaalne-300x174.jpg)

